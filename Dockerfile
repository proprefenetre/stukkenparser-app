# Use Debian b/c of cython dependencies
FROM python:3.8-slim-buster

# System deps:
RUN apt-get update && apt-get -y upgrade && apt-get -y install git
RUN pip3 install poetry

# Copy only requirements to cache them in docker layer
WORKDIR /stukkenparser_app
COPY poetry.lock pyproject.toml /stukkenparser_app/

# Project initialization:
RUN poetry config virtualenvs.create false
RUN poetry install --no-interaction

COPY . .

CMD [ "gunicorn", "-c", "gunicorn.py", "stukkenparser_app:app" ]

