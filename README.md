# Stukkenparse-app

NB: de actieve versie van dit project staat hier: https://gitlab.com/minfin-bbh/stukkenparser-app

Demo-app voor [stukkenparser](https://gitlab.com/proprefenetre/stukkenparser).

## Installatie

Clone de repository:
~~~
$ git clone https://gitlab.com/minfin-bbh/stukkenparser-app
~~~

Als je [docker](https://docker.com) wil gebruiken:

~~~
$ cd stukkenparser-app
$ docker build . -t sp-app
$ docker run -it --name sp-app --rm -p 5000:5000 -v <data volume>:/data: sp-app
~~~

Anders:
~~~
$ cd stukkenparser-app
$ poetry install
...
$ FLASK_APP=stukkenparser_app; flask run
~~~

De app draait nu op `127.0.0.1:5000`.

Voor betere performance kun je een volume aanmaken waar de geparste stukken op worden opgeslagen:

~~~
$ docker volume create sp-data
$ docker run -it --name sp-app --rm -p 5000:5000 -v sp-data:/data: sp-app
~~~
