#!/usr/bin/env python

import json
import html
from pathlib import Path
import os

from appdirs import user_data_dir
from flask import Flask, request, url_for, render_template, send_from_directory
from pygments import highlight
from pygments.lexers.html import XmlLexer
from pygments.formatters import HtmlFormatter
import requests
from stukkenparser import Parser, XMLAdapter


app = Flask(__name__, instance_relative_config=True)

STORAGE = Path("/data/stukkenparser")
try:
    STORAGE.mkdir(exist_ok=True, parents=True)
except PermissionError:
    STORAGE = Path(f"{user_data_dir()}/stukkenparser")

HL_STORE = STORAGE / "highlight"
XML_STORE = STORAGE / "xml"

for path in (HL_STORE, XML_STORE):
    path.mkdir(exist_ok=True, parents=True)

@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "POST":
        doc_id = request.form.get("query")

        doc_url = f"https://zoek.officielebekendmakingen.nl/{doc_id}.html"
        resp = requests.get(doc_url)

        if not resp.ok:
            print(f"Invalid request: {doc_url}")
            return render_template(
                "index.html", invalid=True, query=doc_id)

        print(f"Request: {doc_id}")

        cfg = {
            "embedding": {
                "tussenkop": {
                    "opmaak": {
                        "tussenkop_vet": 2,
                        "tussenkop_cur": 3,
                        "tussenkop_ondlijn": 4,
                        "tussenkop_rom": 5,
                    },
                }
            },
            "types": {
                "kop": {
                    "(?i)(?:[a-z]\\.) algemeen": "algemeen",
                    "(?i)beleidsverslag": "beleidsverslag",
                    "(?i)beleidsprioriteiten": "beleidsprioriteiten",
                    "(?i)\\d\\.? beleidsartikel(en)?": "beleidsartikelen",
                    "(?i)niet-beleidsartikel(en)?": "niet-beleidsartikelen",
                    "(?i)bedrijfsvoeringsparagraaf": "bedrijfsvoeringsparagraaf",
                    "(?i)jaarrekening": "jaarrekening",
                    "(?i)leeswijzer": "leeswijzer",
                    "(?i)beleidsagenda": "beleidsagenda",
                    "(?i)bijlage\\b": "bijlage",
                    "(?i)bijlagen": "bijlagen",
                },
                "tussenkop": {
                    "(?i)beleidsartikel(en)?": "beleidsartikelen",
                    "(?i)niet-beleidsartikel(en)?": "niet-beleidsartikelen",
                    "(?i)bedrijfsvoeringsparagraaf": "bedrijfsvoeringsparagraaf",
                },
                "tabel": {"(?i)budgettaire gevolgen": "b_tabel"},
            },
        }

        parser = Parser(cfg)

        if not (XML_STORE / f"{doc_id}.xml").exists():
            document = parser.parse(resp.content)
            xml = XMLAdapter(document).to_string()
            with (XML_STORE / f"{doc_id}.xml").open('w', encoding="utf-8") as f:
                f.write(xml)

            formatter = HtmlFormatter(style="emacs", nowrap=True)

            xml_hl = highlight(xml, XmlLexer(), formatter)
            with (HL_STORE / f"{doc_id}-highlight.html").open('w', encoding="utf-8") as f:
                f.write(xml_hl)
            print("Cached")

        else:
            with (XML_STORE / f"{doc_id}.xml").open("r", encoding="utf-8") as f:
                xml = f.read()
            with (HL_STORE / f"{doc_id}-highlight.html").open("r", encoding="utf-8") as f:
                xml_hl = f.read()
            print("From cache")
            
        return render_template(
            "index.html",
            query=doc_id,
            parsed=True,
            xml=xml_hl
        )

    return render_template("index.html", xml=None)

@app.route("/download/<doc_id>")
def download(doc_id):
    print("\tDownloaded")
    return send_from_directory(XML_STORE, f"{doc_id}.xml", as_attachment=True)
